const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Withdraw Fees", function () {
    let admin, addr1, addr2,nftmarket,nft ;
    before(async () => {
  
      [admin, addr1, addr2] =   await ethers.getSigners();
  
      const NFTMarket = await ethers.getContractFactory("NFTMarket");
      const NFT = await ethers.getContractFactory("NFT");
      nftmarket = await NFTMarket.deploy();//{value: ethers.utils.parseUnits( "10", "ether" ) });
      await nftmarket.deployed();

      nft = await NFT.deploy(nftmarket.address);
      await  nft.deployed();

  
    }) 

    it("Should list item", async ()=> {
        const iNftId = 1, iNftPrice = 2;
        const txnNFT = await nft.createToken("https://picsum.photos/200/300");

        txnNFT.wait();

       const txnMarketItem = await nftmarket.createMarketItem(nft.address, iNftId, iNftPrice, {
            value : ethers.utils.parseUnits( "0.025", "ether" )
        });

       await txnMarketItem.wait();
       const finAmount = iNftPrice*0.01 + iNftPrice;
       const txnNftSale = await nftmarket.connect(addr1).createMarketSale(nft.address, 1, {
        value : ethers.utils.parseUnits( finAmount.toString(), "ether" )
       });

       await txnNftSale.wait();
       const txnWithdraw = await nftmarket.connect(admin).withdrawFees();
       let txn_receipt =  await txnWithdraw.wait();
       const withdrawAmount = txn_receipt.events[0].args[0].toString();
       expect( ethers.utils.formatUnits( withdrawAmount ) ).to.equal(   (iNftPrice*0.01 + 0.025) + "" );     

    });



})  